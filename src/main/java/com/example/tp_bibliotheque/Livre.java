package com.example.tp_bibliotheque;

import java.util.Vector;

public class Livre {
    public String title;
    public String authorFirstName;
    public String  authorLastName;
    public Edition edition;
    public Vector<String> keywords;



    public Livre(String title, String authorFirstName, String authorLastName, Edition edition, Vector<String> keywords) throws Exception {
        this.title = title;
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
        this.edition = edition;
        if ((keywords.size() > 5) || keywords.isEmpty()){
            this.keywords = keywords;
        }
        else{
            throw new Exception("There must be between 1 and 5 keywords");
        }



    }
}
