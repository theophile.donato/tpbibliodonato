package com.example.tp_bibliotheque;

public class Edition {
    public String editor;
    public Integer isbn;
    public Integer editionYear;
    public Integer nbStocked;

    public Edition (String editor, int isbn, int editionYear, int nbStocked){
        this.editionYear = editionYear;
        this.editor = editor;
        this.isbn = isbn;
        this.nbStocked = nbStocked;

    };
}
